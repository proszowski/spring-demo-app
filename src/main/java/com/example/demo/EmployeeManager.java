package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class EmployeeManager {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public EmployeeManager(EmployeeRepository employeeRepository, DepartmentRepository departmentRepository) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
    }

    public Optional<Employee> findById(Long id){
        return employeeRepository.findById(id);
    }

    public Iterable<Employee> findAll(){
        return employeeRepository.findAll();
    }

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }

    public void deleteById(Long id){
        employeeRepository.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDb(){
        Department department = new Department(1L, "test");
        departmentRepository.save(department);
        Employee employee = new Employee(1L, "Nowacki", "Euzebiusz", new BigDecimal("4000.50"), LocalDate.of(1999, 1, 1), department);
        employee.setDepartment(department);
        Employee anotherEmployee = new Employee(2L, "Nowacki", "Eugeniusz", new BigDecimal("1000"), LocalDate.of(1997, 1, 1), department);
        anotherEmployee.setDepartment(department);
        employeeRepository.save(anotherEmployee);
        employeeRepository.save(employee);
    }

    public Employee add(Employee employee) {
        return employeeRepository.save(employee);
    }

}
