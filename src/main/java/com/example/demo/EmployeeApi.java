package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/employees")
public class EmployeeApi {

    private EmployeeManager employeeManager;

    public EmployeeApi(EmployeeManager employeeManager) {
        this.employeeManager = employeeManager;
    }

    @GetMapping("/all")
    public Iterable<Employee> getAll(){
        return employeeManager.findAll();
    }

    @GetMapping
    public Optional<Employee> getById(@RequestParam Long index){
        return employeeManager.findById(index);
    }

    @PostMapping("/add")
    public Employee add(@RequestBody Employee employee){
        return employeeManager.add(employee);
    }
}
