package com.example.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByFirstName(String firstName);

    @Query(value = "SELECT * FROM Employees where last_name ilike 'k%'", nativeQuery = true)
    List<Employee> findAllWithKAtBeginningInLastName();
}
